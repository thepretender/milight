﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using MiLight.Control;

namespace MiLight
{
    class Program
    {
        private static readonly LightController _controller = new LightController();

        static void Main(string[] args)
        {
            Console.WriteLine("Holy guacamole MiLight controller!");
            Console.WriteLine("Usage:");
            Console.WriteLine("\ts [simple command]");
            foreach (var name in Enum.GetNames(typeof(SimpleCommandType)).Skip(1))
                Console.WriteLine("\t\t" + name);
            Console.WriteLine("\tg [group 1-4] [group command] [brightness(2-27) | color(0-255)]");
            foreach (var name in Enum.GetNames(typeof(GroupCommandType)).Skip(1))
                Console.WriteLine("\t\t" + name);
            Console.WriteLine("\tw to set all groups to white.");
            Console.WriteLine("\tq to exit.");

            while (true)
            {
                Console.Write("> ");
                string command = Console.ReadLine();
                if (string.IsNullOrEmpty(command))
                    continue;

                var parts = command.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 0)
                    continue;

                if (string.Equals(parts[0], "q", StringComparison.OrdinalIgnoreCase))
                    return;
                
                if (string.Equals(parts[0], "s", StringComparison.OrdinalIgnoreCase))
                    SimpleCommand(parts);
                else if (string.Equals(parts[0], "g", StringComparison.OrdinalIgnoreCase))
                    GroupCommand(parts);
                else if (string.Equals(parts[0], "w", StringComparison.OrdinalIgnoreCase))
                    AllWhite();
                else
                    Console.WriteLine("Don't know how to " + parts[0] + ".");
            }
        }

        private static void AllWhite()
        {
            var command = new AllWhiteCommand();
            _controller.ExecuteCommand(command);
            Console.WriteLine("By your command.");
        }

        private static void GroupCommand(string[] parts)
        {
            if (parts.Length < 3)
            {
                Console.WriteLine("Gimme a group number and command type!");
                return;
            }

            int group;
            if (!int.TryParse(parts[1], out group))
            {
                Console.WriteLine("Your group number ain't no good.");
                return;
            }

            GroupCommandType type;
            if (!Enum.TryParse(parts[2], true, out type))
            {
                Console.WriteLine("Ain't getting watcha sayin': " + parts[1]);
                return;
            }

            byte optional = 0;
            if (parts.Length > 3)
                byte.TryParse(parts[3], out optional);
            
            var command = new SimpleCommand(group, type, optional);
            _controller.ExecuteCommand(command);
            Console.WriteLine("By your command.");
        }

        private static void SimpleCommand(string[] parts)
        {
            if (parts.Length < 2)
            {
                Console.WriteLine("Gimme simple command type!");
                return;
            }

            SimpleCommandType type;
            if (!Enum.TryParse(parts[1], true, out type))
            {
                Console.WriteLine("Ain't getting watcha sayin': " + parts[1]);
                return;
            }

            var command = new SimpleCommand(type);
            _controller.ExecuteCommand(command);
            Console.WriteLine("By your command.");
        }
    }
}
