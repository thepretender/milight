﻿namespace MiLight.Control
{
    internal class ColorCommand : CommandBase
    {
        private readonly byte _color;

        public ColorCommand(byte color)
        {
            _color = color;
        }
        
        public override byte[] GetCommand()
        {
            return new byte[] {0x40, _color, 0x55};
        }
    }
}