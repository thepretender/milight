﻿namespace MiLight.Control
{
    public enum PartialCommandType : byte
    {
        None = 0,

        AllWhite = 0xC2,
        Group1White = 0xC5,
        Group2White = 0xC7,
        Group3White = 0xC9,
        Group4White = 0xCB,
    }
}