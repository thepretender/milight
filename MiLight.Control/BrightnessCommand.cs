﻿namespace MiLight.Control
{
    internal class BrightnessCommand : CommandBase
    {
        private readonly byte _brightness = 27;

        public BrightnessCommand()
        {
        }

        public BrightnessCommand(byte brightness)
        {
            if (brightness < 2)
                _brightness = 2;
            else if (brightness > 27)
                _brightness = 27;
            else
                _brightness = brightness;
        }

        public override byte[] GetCommand()
        {
            return new byte[] {0x4E, _brightness, 0x55};
        }
    }
}