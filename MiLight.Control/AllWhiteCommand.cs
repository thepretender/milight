﻿namespace MiLight.Control
{
    public class AllWhiteCommand : SimpleCommand
    {
        public AllWhiteCommand()
        {
            CommandType = 0x42;
            NextCommand = new SimpleCommand {CommandType = 0xC2};
        }
    }
}