﻿namespace MiLight.Control
{
    public enum SimpleCommandType : byte
    {
        None = 0,

        AllOff = 0x41,
        AllOn = 0x42,
        Slower = 0x43,
        Faster = 0x44,
        G1On = 0x45,
        G1Off = 0x46,
        G2On = 0x47,
        G2Off = 0x48,
        G3On = 0x49,
        G3Off = 0x4A,
        G4On = 0x4B,
        G4Off = 0x4C,
        Disco = 0x4D,
    }
}