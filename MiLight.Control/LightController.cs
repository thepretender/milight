﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MiLight.Control
{
    public class LightController
    {
        private readonly UdpClient _client;

        public LightController() : this(IpHelper.GetBroadcastIp())
        {
        }

        public LightController(IPAddress address)
        {
            _client = new UdpClient
                      {
                          EnableBroadcast = true,
                          DontFragment = true
                      };

            _client.Connect(new IPEndPoint(address, 8899));
        }

        public void ExecuteCommand(CommandBase command)
        {
            var cur = command;
            
            do
            {
                var msg = cur.GetCommand();
                if (msg == null)
                    return;

                if (!ReferenceEquals(cur, command))
                    Thread.Sleep(100);

                _client.Send(msg, msg.Length);
            } while ((cur = cur.NextCommand) != null);
        }
    }
}
