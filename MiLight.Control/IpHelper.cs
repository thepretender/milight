﻿using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace MiLight.Control
{
    public class IpHelper
    {
        public static IPAddress GetBroadcastIp()
        {
            var maskIp = GetHostMask();
            var hostIp = GetHostIp();

            if (maskIp == null || hostIp == null)
                return null;

            var maskBytes = maskIp.GetAddressBytes();
            var hostBytes = hostIp.GetAddressBytes();

            var broadcastBytes = new byte[4];

            for (int i = 0; i < 4; i++)
                broadcastBytes[i] = (byte)((hostBytes[i]) | ~(maskBytes[i]));

            return new IPAddress(broadcastBytes);
        }


        private static IPAddress GetHostMask()
        {
            IPAddress hostIp = GetHostIp();
            var address = NetworkInterface.GetAllNetworkInterfaces()
                                   .Where(x => x.OperationalStatus == OperationalStatus.Up)
                                   .SelectMany(x => x.GetIPProperties().UnicastAddresses)
                                   .FirstOrDefault(x => x.Address.Equals(hostIp));
            
            return address == null ? null : address.IPv4Mask;
        }

        private static IPAddress GetHostIp()
        {
            return Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }
    }
}