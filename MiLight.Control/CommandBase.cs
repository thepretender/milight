﻿namespace MiLight.Control
{
    public abstract class CommandBase
    {
        public abstract byte[] GetCommand();

        public CommandBase NextCommand { get; set; }
    }
}