﻿namespace MiLight.Control
{
    public class SimpleCommand : CommandBase
    {
        public SimpleCommand()
        {
        }

        public SimpleCommand(SimpleCommandType commandType)
        {
            CommandType = (byte)commandType;
        }

        public SimpleCommand(PartialCommandType commandType)
        {
            CommandType = (byte)commandType;
        }

        public SimpleCommand(int group, GroupCommandType type, byte optional = 0)
        {
            if (type == GroupCommandType.None)
                return;

            CommandType = (byte)(0x45 + ((@group - 1) * 2));
            if (type == GroupCommandType.Off)
                CommandType += 1;
            else if (type == GroupCommandType.White)
                NextCommand = new SimpleCommand((PartialCommandType)(0xC5 + ((group - 1) * 2)));
            else if (type == GroupCommandType.Brightness)
                NextCommand = new BrightnessCommand(optional);
            else if (type == GroupCommandType.Color)
                NextCommand = new ColorCommand(optional);
        }

        public sealed override byte[] GetCommand()
        {
            if (CommandType == 0)
                return null;

            return new byte[] {CommandType, 0x00, 0x55};
        }

        public byte CommandType { get; set; }
    }
}