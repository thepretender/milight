﻿namespace MiLight.Control
{
    public enum GroupCommandType
    {
        None,
        On,
        Off,
        White,
        Brightness,
        Color
    }
}