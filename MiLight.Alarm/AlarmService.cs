﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MiLight.Control;

namespace MiLight.Alarm
{
    public partial class AlarmService : ServiceBase
    {
        private const int MaxBrightness = 30;

        private readonly LightController _controller = new LightController();

        private Timer _timer;
        private int _ticks = 0;
        private TimeSpan _time;
        private int _maxTicks;
        private bool _working;
        private byte _brightness = 2;

        public AlarmService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _time = TimeSpan.Parse(ConfigurationManager.AppSettings["time"]);
            _maxTicks = int.Parse(ConfigurationManager.AppSettings["maxTicks"]);

            int ticks = int.Parse(ConfigurationManager.AppSettings["tick"]);
            _timer = new Timer(Tick, null, ticks, ticks);
        }

        protected override void OnStop()
        {
            _timer.Dispose();
        }

        private void Tick(object state)
        {
            if (_working)
            {
                _ticks++;
                if (_ticks >= _maxTicks)
                {
                    _ticks = 0;
                    _brightness++;
                    if (_brightness > MaxBrightness)
                    {
                        _working = false;
                        TurnOff();
                        return;
                    }

                    Increase();
                }
            }
            else
            {
                if (DateTime.Now.TimeOfDay.Hours == _time.Hours && DateTime.Now.TimeOfDay.Minutes == _time.Minutes)
                {
                    _working = true;
                    TurnOn();
                }
            }
        }

        private void TurnOff()
        {
            _controller.ExecuteCommand(new SimpleCommand(1, GroupCommandType.Brightness, 2));
            Thread.Sleep(100);
            _controller.ExecuteCommand(new SimpleCommand(1, GroupCommandType.Off));
        }

        private void TurnOn()
        {
            _brightness = 2;
            _controller.ExecuteCommand(new SimpleCommand(1, GroupCommandType.On));
            Thread.Sleep(100);
            _controller.ExecuteCommand(new SimpleCommand(1, GroupCommandType.Brightness, _brightness));
        }

        private void Increase()
        {
            _controller.ExecuteCommand(new SimpleCommand(1, GroupCommandType.Brightness, (byte)_brightness));
        }
    }
}
